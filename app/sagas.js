import { put, take } from 'redux-saga/effects'
import { phaseTwo, phaseThree, SET_DATA_1, SET_DATA_2 } from 'actions';

export default function* rootSaga() {
  while (true) {

    // Wait for SET_DATA_1 action
    const action = yield(take(SET_DATA_1));

    // We can access the action for SET_DATA_1 to examine and use payload for next actions in the saga.

    // Send request to get more info
    yield(put(phaseTwo()));

    // Wait for the result
    yield(take(SET_DATA_2));

    // Send request to get more info
    yield(put(phaseThree()));
  }
}