import { createAction } from 'redux-actions';

export const API              = 'API';
export const API_START        = 'API_START';
export const API_ERROR        = 'API_ERROR';
export const API_DONE         = 'API_DONE';
export const SET_AUTH_TOKEN   = 'SET_AUTH_TOKEN';
export const CLEAR_AUTH_TOKEN = 'CLEAR_AUTH_TOKEN';

export const SET_USER         = 'SET_USER';
export const GET_DATA_1       = 'GET_DATA_1';
export const GET_DATA_2       = 'GET_DATA_2';
export const GET_DATA_3       = 'GET_DATA_3';
export const SET_DATA_1       = 'SET_DATA_1';
export const SET_DATA_2       = 'SET_DATA_2';
export const SET_DATA_3       = 'SET_DATA_3';

const createApiAction = (url, success, method = 'GET') => params => ({
  type: API,
  payload: { method, url, success, params }
});

export const setAuthToken   = createAction(SET_AUTH_TOKEN);
export const clearAuthToken = createAction(CLEAR_AUTH_TOKEN);

export const getRequest     = createApiAction('user', SET_USER);
export const postRequest    = createApiAction('user', SET_USER, 'POST');
export const multiPhase     = createApiAction('info1', SET_DATA_1);
export const phaseTwo       = createApiAction('info2', SET_DATA_2);
export const phaseThree     = createApiAction('info3', SET_DATA_3);



