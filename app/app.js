import { api as bdsmAPI } from 'bdsmjs';
import mocks from 'raw!../mocks.json';
import store from 'store';
import * as actions from 'actions';

bdsmAPI.import(mocks);

const buttonMap = {
  'get':       actions.getRequest,
  'post':      () => actions.postRequest({ name: 'me' }),
  'multi':     actions.multiPhase,
  'setAuth':   actions.setAuthToken,
  'clearAuth': actions.clearAuthToken
};

const pendingCount = document.getElementById('pending');

Object.keys(buttonMap).forEach(id => document
  .getElementById(id)
  .addEventListener('click', () => store.dispatch(buttonMap[id]()), false)
);

store.subscribe(() => pendingCount.innerText = store.getState().requests);