import { handleActions } from 'redux-actions';
import { API_START, API_DONE, API_ERROR, SET_AUTH_TOKEN, CLEAR_AUTH_TOKEN } from 'actions';

const initialState = {
  requests: 0,
  currentUser: null
};

const set = (state, update) => Object.assign({}, state, update);

export default handleActions({
  [API_START]:        (state) => set(state, { requests: state.requests + 1 }),
  [API_DONE]:         (state) => set(state, { requests: state.requests - 1 }),
  [API_ERROR]:        (state) => set(state, { requests: state.requests - 1 }),
  [SET_AUTH_TOKEN]:   (state) => set(state, { currentUser: { accessToken: 'X' }}),
  [CLEAR_AUTH_TOKEN]: (state) => set(state, { currentUser: { accessToken: undefined }})
}, initialState);
