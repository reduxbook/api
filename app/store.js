import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'

import rootReducer   from 'reducer';
import logMiddleware from 'middleware/log';
import apiMiddleware from 'middleware/api';

import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(
  logMiddleware,
  apiMiddleware,
  sagaMiddleware
));

sagaMiddleware.run(rootSaga);

export default store;