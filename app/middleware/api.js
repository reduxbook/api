import { API, API_START, API_ERROR, API_DONE, } from 'actions';

const API_ROOT = 'http://redux-book.com/code/api/';

const apiMiddleware = ({ dispatch, getState }) => next => action => {
  const { type, payload, meta } = action;

  function setupRequest({ url, method = 'GET', body }) {
    const { accessToken } = getState().currentUser || {};
    const { skipAuth } = meta || {};
    const { pending } = payload;
    const params = { method };

    // Notify system that an API request has started
    dispatch({ type: pending ? pending : API_START });

    if (!skipAuth && accessToken) {
      params.headers = { accessToken }
    }

    if (method === 'POST') {
      params.body = JSON.stringify(body);
    }

    return fetch(API_ROOT + url, params);
  }

  const checkStatus = response => {
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  };

  const handleResponse = data => {
    const { success } = payload;

    dispatch({ type: success, payload: data });

    dispatch({ type: API_DONE });
  };

  const handleError = error => {
    const errorAction = payload.error;

    if (errorAction) {
      dispatch({ type: errorAction, error });
    }

    dispatch({ type: API_ERROR, error });
  };

  switch (type) {
    case API:
      setupRequest(payload)
        .then(checkStatus)
        .then(response => response.json())
        .then(handleResponse)
        .catch(handleError);
      break;
  }

  return next(action);
};

export default apiMiddleware;