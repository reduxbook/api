const logMiddleware = ({}) => next => action => {
  console.log(action.type, action);

  next(action);
};

export default logMiddleware;